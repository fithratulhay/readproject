@extends('layouts.app')

@section('title')
    Dashboard    
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">My Articles</div>
                
                <div class="card-body">
                    @foreach ($articles as $article)
                        <li> 
                            <a href="/articles/{{ $article->id }}"> {{$article->title}} </a> 
                        </li>                            
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
