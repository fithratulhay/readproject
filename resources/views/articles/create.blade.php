@extends('layouts.app')

@section('title')
    Create New Article
@endsection

@section('content')
    <h1 class="title">CREATE A NEW ARTICLE</h1>

    <form method="POST" action="/articles">
        @csrf

        <div class="field">
            <label class="label" for="title">Title</label>
    
            <div class="control">
                <input type="text" class="input {{$errors->has('title') ?'is-danger' : ''}}" name="title" placeholder="Article title" value="{{ old('title')}}" required>
            </div>
        </div>

        <div class="field">
            <label class="label" for="description">Description</label>
    
            <div class="control">
                <textarea name="description" class="textarea {{$errors->has('description') ?'is-danger' : ''}}" placeholder="Article description" required></textarea>
            </div>
        </div>


        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link">Create Article</button>
            </div>
        </div>

        @if ($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{ $error }} </li>
                    @endforeach
                </ul>
            </div>
        @endif

    </form>

    
@endsection