@extends('layouts.app')

@section('title')
    {{$article->title}}
@endsection

@section('content')
    
    <h1 class="title">{{$article->title}}</h1>    
    
    <div class="content">
        {{ "by ".$article->user->name  }}
        
        <br>
        <br>
        
        {{$article->description}}
    </div>

    @if ($article->user_id == $auth->id())
        <a href="/articles/{{$article->id}}/edit">Edit this article</a>
    @endif
    
    <br>
    <br>

    <div class="card">
        <div class="card-header">Comments</div>
        
        <div class="card-body">
            @auth
                <form method="POST" action="/articles/{{$article->id}}/comments" style="margin-bottom : 0.5cm">
                    @method('PATCH')
                    @csrf
                    
                    <div class="field">
                        <label class="label" for="title">Add Comment</label>
                    
                        <div class="control">
                            <input type="text" class="input" name="comment" placeholder="Comment" value="Comment" required>
                        </div>
                    </div>

                    <br>
                    
                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-link">Add</button>    
                        </div>
                    </div>
                
                    @if ($errors->any())
                        <div class="notification is-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li> {{ $error }} </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                </form>
            @else
                <a href="/articles/{{$article->id}}/comments"> Add your comment </a>
            @endauth
    
            @foreach ($article->comments as $comment)
                <li>
                    {{$comment->comment}}, by {{$comment->user->name}} 

                    @auth
                        @if ($comment->user_id == auth()->user()->id)
                            <form method="POST" action="/comments/{{$comment->id}}">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="button">Delete Comment</button>    
                            </form>                    
                        @endif
                    @endauth 
                </li>
            @endforeach
        </div>
    </div>

    <li>
        <a href="/articles">See all articles</a>
    </li>
@endsection