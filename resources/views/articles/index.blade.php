@extends('layouts.app')

@section('title')
    Articles
@endsection

@section('content')
    <h1 class="title">ARTICLES</h1>

    
        @foreach ($articles as $article)
            <li>
                <a href="/articles/{{$article->id}}">
                    {{ $article->title }}
                </a>
            </li>
        @endforeach
    

    <br>
    <br>

    <li><a href="/articles/create">Create New Article</a></li>
@endsection
