<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Comment;

class ArticleCommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function verify(Article $article){
        return redirect("/articles/$article->id");
    }

    public function add(Article $article){
        $attribute = request()->validate([
            'comment' => 'required'
        ]);
        $attribute['user_id'] = auth()->user()->id;
        $article->addComment($attribute);

        return back();
    }

    public function destroy(Comment $comment){
        $comment->delete();

        return back();
    }
}
